import org.gradle.api.artifacts.dsl.DependencyHandler

object Dependencies {
    const val COMPOSE_UI = "androidx.compose.ui:ui:${Versions.COMPOSE}"
    const val COMPOSE_UI_TOOLING = "androidx.compose.ui:ui-tooling:${Versions.COMPOSE}"
    const val COMPOSE_UI_TOOLING_PREVIEW =
        "androidx.compose.ui:ui-tooling-preview:${Versions.COMPOSE}"
    const val ACTIVITY_COMPOSE = "androidx.activity:activity-compose:${Versions.ACTIVITY_COMPOSE}"
    const val COMPOSE_MATERIAL = "androidx.compose.material:material:${Versions.COMPOSE}"

    // DI
    const val HILT = "com.google.dagger:hilt-android:${Versions.HILT}"
    const val HILT_COMPILER = "com.google.dagger:hilt-android-compiler:${Versions.HILT}"

    // retrofit
    const val RETROFIT = "com.squareup.retrofit2:retrofit:${Versions.RETROFIT}"
    const val RETROFIT_CONVERTER = "com.squareup.retrofit2:converter-gson:${Versions.RETROFIT}"

    // Testing
    const val JUNIT = "junit:junit:${Versions.JUNIT}"
    const val MOCKK = "io.mockk:mockk:${Versions.MOCKK}"

    const val COIL = "io.coil-kt:coil-compose:${Versions.COIL}"
}

fun DependencyHandler.hilt() {
    add("implementation", Dependencies.HILT)
    add("kapt", Dependencies.HILT_COMPILER)
}

fun DependencyHandler.retrofit() {
    add("implementation", Dependencies.RETROFIT)
    add("implementation", Dependencies.RETROFIT_CONVERTER)
}

/**
 * Implement all dependencies for jetpack compose.
 * @param withPreviewImplemented Boolean to debugImplement the compose UI tooling lib.
 * @param useMaterial Define if use Compose material lib. (Used by default)
 */
fun DependencyHandler.compose(
    withPreviewImplemented: Boolean,
    useMaterial: Boolean = true
) {
    add("implementation", Dependencies.COMPOSE_UI)
    add("implementation", Dependencies.COMPOSE_UI_TOOLING_PREVIEW)
    add("implementation", Dependencies.ACTIVITY_COMPOSE)

    if (useMaterial) {
        add("implementation", Dependencies.COMPOSE_MATERIAL)
    }

    if (withPreviewImplemented) {
        add("debugImplementation", Dependencies.COMPOSE_UI_TOOLING)
    }
}

fun DependencyHandler.unitTesting() {
    add("testImplementation", Dependencies.MOCKK)
    add("testImplementation", Dependencies.JUNIT)
}
