object Versions {
    const val COMPOSE = "1.2.0"
    const val HILT = "2.43.2"
    const val ACTIVITY_COMPOSE = "1.5.1"

    // Testing
    const val JUNIT = "4.13.2"
    const val MOCKK = "1.12.3"

    const val SERIALIZATION_PLUGIN = "1.7.20"
    const val RETROFIT = "2.9.0"

    const val COIL = "2.0.0-rc01"
}
