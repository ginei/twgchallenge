# TWG Challenge
Este es un proyecto creado para demostrar algunas skills que poseo.

El desafio se trata de consumir la siguiente url:
https://api.chucknorris.io/
Se debe consumir 20 veces y mostrarlos.

## Tech
- [Compose] : <https://developer.android.com/jetpack/compose/documentation>
- [Hilt] : <https://developer.android.com/training/dependency-injection/hilt-android>
- [Retrofit] : <https://square.github.io/retrofit>
- [Mvvm] : <https://developer.android.com/jetpack/guide?gclid=CjwKCAjwp9qZBhBkEiwAsYFsbzyS7E6A6Kh__9pHP441KGASm02rIhhDtSSZGuMtMDrL9BlGVEZPxhoCiZIQAvD_BwE&gclsrc=aw.ds>
- [Coil] : <https://github.com/coil-kt/coil>
- [Mockito] : <https://site.mockito.org>
