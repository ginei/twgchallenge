package com.ginei.myapplication.di

import android.app.Application
import com.ginei.myapplication.data.controllers.ChuckNorrisController
import com.ginei.myapplication.data.controllers.ChuckNorrisControllerImpl
import com.ginei.myapplication.data.repositories.ChuckNorrisRemoteDataSource
import com.ginei.myapplication.data.repositories.ChuckNorrisRepository
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class ChuckModules {

    @Provides
    fun provideController(repository: ChuckNorrisRepository): ChuckNorrisController {
        return ChuckNorrisControllerImpl(repository)
    }

    @Provides
    fun provideRepository(service: ChuckNorrisRemoteDataSource): ChuckNorrisRepository {
        return ChuckNorrisRepository(service)
    }

    @Provides
    fun provideService(retrofit: Retrofit): ChuckNorrisRemoteDataSource {
        return retrofit.create(ChuckNorrisRemoteDataSource::class.java)
    }

    @Provides
    @Singleton
    fun provideOkhttpClient(cache: Cache): OkHttpClient {
        val client = OkHttpClient.Builder()
        client.cache(cache)
        return client.build()
    }

    @Provides
    @Singleton
    fun provideOkHttpCache(application: Application): Cache {
        return Cache(application.cacheDir, 10240L)
    }

    @Provides
    fun service(
        httpClient: OkHttpClient,
        gson: Gson,
    ): Retrofit {
        return Retrofit.Builder()
            .callFactory { httpClient.newCall(it) }
            .baseUrl("https://api.chucknorris.io/")
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    @Provides
    fun providesGson(): Gson {
        return Gson()
    }
}
