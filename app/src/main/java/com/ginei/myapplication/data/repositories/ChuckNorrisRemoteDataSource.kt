package com.ginei.myapplication.data.repositories

import com.ginei.myapplication.data.ChuckNorrisModel
import retrofit2.http.GET

interface ChuckNorrisRemoteDataSource {

    @GET("/jokes/random")
    suspend fun getChuckNorris(): ChuckNorrisModel
}
