package com.ginei.myapplication.data

import com.google.gson.annotations.SerializedName

class ChuckNorrisModel(
    @SerializedName("value")
    val value: String,
    @SerializedName("icon_url")
    val iconUrl: String,
    @SerializedName("url")
    val url: String
)
