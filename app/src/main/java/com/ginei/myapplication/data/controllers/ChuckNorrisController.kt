package com.ginei.myapplication.data.controllers

import com.ginei.myapplication.data.ChuckNorrisModel

interface ChuckNorrisController {

    suspend fun getChuckNorris(): List<ChuckNorrisModel>
}
