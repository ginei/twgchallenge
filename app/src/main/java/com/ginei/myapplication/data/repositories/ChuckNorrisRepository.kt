package com.ginei.myapplication.data.repositories

import com.ginei.myapplication.data.ChuckNorrisModel

class ChuckNorrisRepository constructor(private val service: ChuckNorrisRemoteDataSource) {

    suspend fun getChuckNorris(): ChuckNorrisModel {
        return service.getChuckNorris()
    }
}
