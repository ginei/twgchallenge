package com.ginei.myapplication.data.controllers

import com.ginei.myapplication.data.ChuckNorrisModel
import com.ginei.myapplication.data.repositories.ChuckNorrisRepository
import javax.inject.Inject

class ChuckNorrisControllerImpl @Inject constructor(
    private val repository: ChuckNorrisRepository
) : ChuckNorrisController {

    override suspend fun getChuckNorris(): List<ChuckNorrisModel> {
        val list = arrayListOf<ChuckNorrisModel>()
        for (i in 1..20) {
            repository.getChuckNorris().let {
                list.add(it)
            }
        }
        return list
    }
}
