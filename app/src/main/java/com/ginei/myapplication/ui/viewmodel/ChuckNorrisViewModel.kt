package com.ginei.myapplication.ui.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ginei.myapplication.data.controllers.ChuckNorrisController
import com.ginei.myapplication.data.ChuckNorrisModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ChuckNorrisViewModel @Inject constructor(
    private val chuckNorrisController: ChuckNorrisController
) : ViewModel() {

    private val _state = MutableStateFlow<ChuckNorrisUIStates>(ChuckNorrisUIStates.Loading(false))
    val state: SharedFlow<ChuckNorrisUIStates> = _state

    init {
        viewModelScope.launch {
            chuckNorrisController.getChuckNorris().let {
                _state.emit(ChuckNorrisUIStates.Data(it))
            }
        }
    }

    sealed class ChuckNorrisUIStates {
        data class Loading(val show: Boolean) : ChuckNorrisUIStates()
        data class Data(val values: List<ChuckNorrisModel>) : ChuckNorrisUIStates()
    }
}
