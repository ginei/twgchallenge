package com.ginei.myapplication.ui.activities

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.ginei.myapplication.data.ChuckNorrisModel
import com.ginei.myapplication.ui.theme.MyApplicationTheme
import com.ginei.myapplication.ui.views.CardItem
import com.ginei.myapplication.ui.views.Loader
import com.ginei.myapplication.ui.viewmodel.ChuckNorrisViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    private val viewModel: ChuckNorrisViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MyApplicationTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    viewModel.state.collectAsState(
                        initial = ChuckNorrisViewModel.ChuckNorrisUIStates.Loading(
                            false
                        )
                    ).let {
                        if (it.value is ChuckNorrisViewModel.ChuckNorrisUIStates.Data) {
                            Content((it.value as ChuckNorrisViewModel.ChuckNorrisUIStates.Data).values)
                        } else {
                            Loader()
                        }
                    }
                }
            }
        }
    }
}

@Composable
private fun Content(value: List<ChuckNorrisModel>) {
    LazyColumn {
        items(value) { item ->
            CardItem(item.value, item.iconUrl, item.url)
            Spacer(modifier = Modifier.width(8.dp))
        }
    }
}
