package com.ginei.myapplication.ui.views

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.SnackbarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import com.ginei.myapplication.ui.theme.Purple200

@Composable
fun Loader() {
    Box(
        modifier = Modifier
            .clickable(false) { /* Nothing */ }
            .fillMaxSize()
            .background(SnackbarDefaults.backgroundColor),
        contentAlignment = Alignment.Center
    ) {
        CircularProgressIndicator(
            color = Purple200,
        )
    }
}
