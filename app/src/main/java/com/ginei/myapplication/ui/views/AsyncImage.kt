package com.ginei.myapplication.ui.views

import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImagePainter
import coil.compose.rememberAsyncImagePainter
import coil.request.ImageRequest

@Composable
fun AsyncImage(urlImage: String) {
    val modifierBuilder = Modifier
        .size(40.dp)
        .clip(CircleShape)
        .border(1.5.dp, MaterialTheme.colors.secondaryVariant, CircleShape)

    val painter = rememberAsyncImagePainter(
        model = ImageRequest.Builder(LocalContext.current)
            .data(urlImage)
            .crossfade(true)
            .build(),
        contentScale = ContentScale.Fit
    )

    // If there is an error state when loading the image.
    if (painter.state is AsyncImagePainter.State.Error) {
        ErrorBoxImage(
            modifier = modifierBuilder
        )

    } else {
        Image(
            modifier = modifierBuilder,
            painter = painter,
            contentDescription = ""
        )
    }
}
