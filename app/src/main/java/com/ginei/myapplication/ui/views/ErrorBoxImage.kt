package com.ginei.myapplication.ui.views

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.SnackbarDefaults
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.ginei.myapplication.R

@Composable
fun ErrorBoxImage(
    modifier: Modifier = Modifier,
) {
    Surface(
        modifier = modifier,
        color = SnackbarDefaults.backgroundColor,
        shape = RectangleShape,
        elevation = 4.dp
    ) {
        Image(
            modifier = Modifier
                .fillMaxSize()
                .padding(14.dp),
            painter = painterResource(id = R.drawable.ic_launcher_foreground),
            contentDescription = ""
        )
    }
}
