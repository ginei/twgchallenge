package com.ginei.myapplication

import com.ginei.myapplication.data.ChuckNorrisModel
import com.ginei.myapplication.data.controllers.ChuckNorrisController
import com.ginei.myapplication.data.controllers.ChuckNorrisControllerImpl
import com.ginei.myapplication.data.repositories.ChuckNorrisRemoteDataSource
import com.ginei.myapplication.data.repositories.ChuckNorrisRepository
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class ControllerTest {

    @MockK
    lateinit var repository: ChuckNorrisRepository

    private lateinit var controller: ChuckNorrisController

    @Before
    fun before() {
        MockKAnnotations.init(this)
        repository = mockk()
        controller = ChuckNorrisControllerImpl(repository)
    }

    @Test
    fun requestingServiceInvoke() {

        val mockedResponse = mockk<ChuckNorrisModel>()
        coEvery { repository.getChuckNorris() } returns mockedResponse

        val response = runBlocking { repository.getChuckNorris() }
        coVerify { repository.getChuckNorris() }
        assertEquals(response, mockedResponse)
    }
}
