package com.ginei.myapplication

import com.ginei.myapplication.data.ChuckNorrisModel
import com.ginei.myapplication.data.repositories.ChuckNorrisRemoteDataSource
import com.ginei.myapplication.data.repositories.ChuckNorrisRepository
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class RepositoryTest {

    @MockK
    lateinit var service: ChuckNorrisRemoteDataSource

    private lateinit var repository: ChuckNorrisRepository

    @Before
    fun before() {
        MockKAnnotations.init(this)
        service = mockk()
        repository = ChuckNorrisRepository(service)
    }

    @Test
    fun requestingServiceInvoke() {
        //given
        val mockedResponse = mockk<ChuckNorrisModel>()
        coEvery { service.getChuckNorris() } returns mockedResponse
        //when
        val response = runBlocking { repository.getChuckNorris() }
        // then
        assertEquals(response, mockedResponse)
    }
}
