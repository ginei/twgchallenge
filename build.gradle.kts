buildscript {
    repositories {
        google()
        mavenCentral()
    }
    dependencies {
        classpath("com.google.dagger:hilt-android-gradle-plugin:2.43.2")
    }
}
//// Top-level build file where you can add configuration options common to all sub-projects/modules.
plugins {
    id("com.android.application")  apply false
    id("com.android.library") apply false
    id("org.jetbrains.kotlin.android") apply false
}

tasks.register("clean", Delete::class) {
    delete(rootProject.buildDir)
}

//configurations.all {
//    resolutionStrategy.eachDependency {
//        if (requested.name.contains("javapoet")) {
//            useVersion("1.13.0")
//        }
//    }
//}
